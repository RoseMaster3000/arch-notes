=============================================
The GUI, aka "Desktop Environment"
============================================
With our package managers installed, we can begin installing software.
The first thing we will be installing is the "Desktop Environment".
Before we proceed, lets review the Desktop Environment subcomponents / lingo.

    (DE) Desktop Environment: Examples include GNOME, KDE, LXDE, and Xfce...A Desktop Environment is a bundle of programs aimed at covering things "everyone" needs. this induces a DS, a WM, and many other programs that cover a swath of common computing tasks (file manager, web browser, etc). These other programs often share a common WT.

    (DS) Display Server: Examples include Xorg, Wayland...a program that draws graphics to the screen.

    (WM) Windows Manager: Examples include i3, awesome, bspwm...a program that graphically manages/containerizes programs inside windows

    (WT) Widget Toolkits: Examples of these include GTK,Qt,KDE...These are tools developers use to make GUI's for their programs. When multiple programs share the same GUI Widget Toolkit, the system has a unified look, as everything is constructed from the same looking buttons, drop-downs, etc. Also, a "system wide theme" is possible by editing singular Widget Toolkit config file.

    (DM/GDM) Graphical Display Manager: program your computer runs immediately after booting, pretty much a login screen program. If you install multiple Desktop Environments, you can both choose the user and DE you'd like to login as. This is technically optional, since you can just login from the Distro's raw tty terminal.

    (Compositors) Examples include picom/wayfire. This is an optional component that complements your DS (display server) by leveraging your GPU to do cool visual effects (window blurring, shadows, etc.)


There are many pre-established DE's you can install to have all of these components pre-selected for you (If your hardware is really old/bad, I recommend LXQt or MATE)

That being said, I won't be installing a pre-established DE. Instead, I will be homebrewing my own "custom" DE by installing each component by hand.
After all, why install what "everyone" needs, when you just install exactly what *YOU* need.


We will be:
    - Making sure out Graphics/Sound Drivers are installed
    - Installing a DS (Xorg)
    - Installing a WM (i3)

=============================================
Video Drivers 
============================================

Make sure everything is up to date
    > pacamn -Syyuu
    > yay -Syyuu
    > paru -Syyuu

Deduce what Graphic Card / Sound Card we have
    > lspci -v | grep -A1 -e VGA -e 3D

If Nvidia/Intel Hybrid Laptop
    https://wiki.archlinux.org/title/NVIDIA_Optimus

If NVIDIA GPU
    The Open Source Nvidia driver (nouvea) 
        Quite good for for older GPUs!
            (~700 series or older)
        Finicky/glitchy on newer GPUs...
            Nvidia Drivers went open source in May 2022!
            Nouvea should become the best NVIDIA driver in the future (~2023?)

    Proprietary Nvidia driver is the way to go for now...
        Has the bleeding edge features
        Begudgingly use this driver if you are running a modern GPU
            (~900 series or newer)
        "So Nvidia... fuck you!" -Linus Torvalds

    Driver
        > paru nvidia                    (core proprietary driver)
        > paru nvidia-lts                (if you are using linux-lts)
        > paru nvidia-beta               (if you are using a custom kernel?)
    OpenGL
        > paru nvidia-utils          (OpenGL)
        > paru lib32-nvidia-utils    (OpenGL, 32-bit backwards comaptability)
    Cuda
        > paru cuda             (Nvidia GPU programming toolkit)
        > paru cuda-tool        (Nvidia GPU programming utilites: nvvp, nsight, samples)
        > paru cudnn            (NVidia CUDA Deep Nueral Network Library)
        > paru cuda             (See the many wrappers/libraries...)
    Settings
        > paru nvidia-settings           (settings tool + GUI tool)
        > nvidia-settings --load-config-only  (generate config file)

    Blacklist nouveau (optional)
        It may be wise to blacklist the open soruce nvidia drivers so there is no conflict
        > nano /usr/lib/modprobe.d/nvidia.conf  (APPEND)
            blacklist nouveau
        > nano /etc/modprobe.d/nvidia.conf      (APPEND)
            blacklist nouveau

    SWTICH DRIVER + VERIFY
        > reboot
        > lspci -v | awk "/VGA/,/^$/"
            (see kernel module in use + available kernel modules)

    Tweak Nvidia Driver (optional)
        > paru nvidia-tweaks
        https://github.com/illnyang/nvlax

    Fix Monitor Wake Issues? (optional)
        https://bbs.archlinux.org/viewtopic.php?id=260159
        https://ask.fedoraproject.org/t/display-adapter-wont-wake-up-after-suspend/16834/3


If Intel+NVIDIA Optimus GPU / INTEL GPU (discrete)
    I have no idea...
    https://wiki.archlinux.org/title/NVIDIA_Optimus
    > paru intel-gpu-tools (?)


If INTEL GPU (integrated) 
    > pacman mesa          (OpenGL)
    > pacman lib32-mesa    (OpenGL, 32-bit backwards compatibility) 
    
    Graphics μMicro Controller (GuC & HuC)
        requries [> paru linux-firmware] (should already be installed during pacstrap)

        (HuC) Hardware Video Acceleration 
            introduced in Gen 9+ CPUs (Coffee Lake+)
            > paru intel-media-driver
                https://github.com/intel/media-driver/
            to use video acceleration on YouTube...
                you to watch in an external video player (eg, VLC)
                to do this, you will need a browser extension
                eg, to send a YouTube video in FireFox to MPV player:
                https://addons.mozilla.org/en-US/firefox/addon/send-to-mpv-player/

        (GuC) Scheduling/Power Management
            introduced in Gen 12+ CPUs (Alder Lake-P Mobile+)
            use GPU to make intelligent power saving decisions

        (FBC) Frame Buffer Compression
            works best on Gen 6+ CPUs (Sandy Bridge+)
            saves power / memory bandwidth with screen refreshing

        (fastboot) Intel Fastboot
            preserve frame buffer during boot
            should prevent screen flickering

        Configure Kernel
            do NOT have "nomodeset" in kernel
                (Intel requires kernel mode setting)
            See kernel options
                > modinfo -p i915

            > nano /etc/modprobe.d/i915.conf
                options i915 enable_guc=2
                options i915 enable_fbc=1
                options i915 fastboot=1
            enable_guc : replace "2" with relevent number
                0: nothing
                1: GuC only
                2: HuC only
                3: GuC and HuC
            enable_fbc : replace "1" with relevent number
                0: off
                1: on
            fastboot : replace "1" with relevent number
                0: off
                1: on

        Rebuild initramfs
            for existing presets/kernel
                > mkinitcpio -P
            for specific kernel
                > mkinitcpio -p linux-lts

        Put into effect
            > reboot

        Verify settings
            > dmesg | grep "GuC"
            > dmesg | grep "HuC"
            failure messages:
                "GuC is not supported!"
                "HuC is not supported!"
            success messages:
                "GuC firmware i915/icl_guc_33.0.0.bin version 33.0"
                "HuC firmware i915/icl_huc_9.0.0.bin version 9.0"


Intel Generations
    there is overlap, for better list, go here:
    https://en.wikipedia.org/wiki/Intel_Core
    6th  : Broadwell / Skylake
    7th  : Skylake / Kaby Lake
    8th  : Kaby / Whisky / Cannon /  ...
    9th  : Coffee Lake / Skylake
    10th : Cascade / Ice / Comet / Amber ...
    11th : Tiger Lake / Rocket Lake
    12th : Alder Lake
  

If "Generic" Video Card
    https://man.archlinux.org/man/extra/xf86-video-vesa/vesa.4.en
    > pacman xf86-video-vesa (?)
    > reboot (to put into effect)


=============================================
Display Server (DS)
============================================

Xorg (DS)
    Install basic xorg (and a couple basic GUI programs)
        > paru xorg-server   (Xorg display server)
        > paru xorg-xinit    (Xorg config file based initializer)
        > paru xterm         (Xorg GUI terminal program)
    Test xorg
        > startx
        (will launch xorg with a window manager called "twm," "Toms Window manager" circa 1987)
    Quit Xorg
        Run command in one of the GUI terminals (xterm)
        > killall Xorg
        *NOTE* proprietary Nvidia Drivers may cause black screen after killing Xorg
               The simplist way to fix this is to unplug/replug your HDMI cable.


Configure Xorg + NVIDIA
    Config file is one of these:
        /etc/X11/xorg.conf
        /etc/X11/xorg.conf.d/20-nvidia.conf   (preferred?)

    generate config file
        > nvidia-xconfig

    edit config file (use GUI tool to correct resoltion/refresh rate)
        > startx
        > su
        > nvidia-settings (GUI that generates ~/.nvidia-settings-rc)
            -> X Server Display Configuration... edit resolution, refresh-rate, etc...
            -> X Server Display Configuration -> Save to X Configuration (do not merge)
        > exit
        > killall Xorg

    VERIFY (make sure direct rendering is happening)
        > startx
        > cat /var/log/Xorg.0.log | grep dri
            OR
        > cat ~/.local/share/xorg/Xorg.0.log | grep dri
        > killall Xorg


Configure Xorg + Intel (integrated graphics)
    > paru xf86-video-intel  (Xorg graphics driver)

    Config file is one of these:
        /etc/X11/xorg.conf
        /etc/X11/xorg.conf.d/20-intel.conf   (preferred?)
            Section "Device"
                Identifier "Intel Graphics"
                Driver "intel"
            EndSection


        https://man.archlinux.org/man/intel.4
        consider explicitly setting acceleration if one is acting up
            (in Driver section)
            Option  "AccelMethod"  "sna"   (default)
            Option  "AccelMethod"  "uxa"
            Option  "AccelMethod"  "blt"
            
        SNA method can cause video tearing...
        consider explicitly setting the following
            (in Device section)
            Option "TearFree" "true"

        DRI3 (default) can cause artifacts when switching virtual desktops
            Option "DRI" "2"



    VERIFY (check if direct rendering)
        > startx
        > cat /var/log/Xorg.0.log | grep dri
            OR
        > cat ~/.local/share/xorg/Xorg.0.log | grep dri
        > killall Xorg


Xorg Sleep Configurations
    https://wiki.archlinux.org/title/Display_Power_Management_Signaling
    https://www.x.org/releases/X11R7.7/doc/man/man5/xorg.conf.5.xhtml#heading16

        > nano /etc/X11/xorg.conf
            Section "ServerLayout"
                Identifier     "Layout0"
                ...
                Option "BlankTime" "0"
                Option "OffTime" "30"
                Option "StandbyTime" "45"
                Option "SuspendTime" "60"
                ...
            EndSection

        HARDCODE disable sleeping
            > paru xset
            > xset s off
            > nano ~/.xinitrc   (APPEND)
                xset s off

    Sleep settings explained:
        Option "BlankTime" 10
            Default: 10 minutes.
            inactivity timeout minutes for the blank phase of the screensaver.
            Blank == monitor blacks out
        Option "OffTime" 0
            THIS FEATURE DOESNT WORK; IT JUST TRIGGERS "blank phase" INSTEAD
            Default: 10 minutes.
            sets the inactivity minutes timeout for the off phase of DPMS mode.*
            Off == monitor shuts down.
        Option "StandbyTime" 30
            Default: 10 minutes
            inactivity timeout minutes for the standby phase of DPMS mode.*
            Standby == computer's state is put onto the RAM and devices are shut down. (aka sleep) 
        Option "SuspendTime" 40
            Default: 10 minutes.
            inactivity timeout minutes for the suspend phase of DPMS mode.*
            Suspend == comptuer's state is put onto the hard drive (swap partition) and PC is shut down. (aka hibernate)


        https://www.x.org/releases/X11R7.7/doc/man/man5/xorg.conf.5.xhtml#heading5
            * Value can be changed at run−time with xset (>paru xset)
            * Only suitable for VESA DPMS compatible monitors (might not support your video driver)
            * It is only enabled for screens that have the "DPMS" option set
            * 0 == timeout is disabled


=============================================
Window Manager (WM)
============================================

I will be installing i3
    https://i3wm.org/
    simple but powerful
    tiling window manager (use all screen real-estate)

alternative window managers include...
https://wiki.archlinux.org/title/Comparison_of_tiling_window_managers
    awesome
        many features
        https://awesomewm.org/
    dwm
        very minimal
        https://dwm.suckless.org/tutorial/
    qTile
        very hackable, written in python
        http://www.qtile.org/


Window Manager installation (i3)
    > paru i3-wm   (also consider i3-gaps)
    > paru i3status  (status bar)

    VERIFY
        > startx i3    (DO NOT RUN AS ROOT)
            (you will go though an i3 config wizard, assing your mod key, etc)
            (might need to reboot to get this to work)

    Learn these default i3 hot-keys:
        mod+enter      OPEN xterm
        mod+shift+q    QUIT current focused application
        mod+shift+e    EXIT i3

    Automatic GUI launching
        have i3 run when X11 is launched (xinit)
            > cp /etc/X11/xinit/xinitrc ~/.xinitrc
            > nano ~/.xinitrc   
                delete tom's window manager stuff at bottom of file
                    ("twm &" line to end of file)
                append our new window manager stuff to bottom of file
                    exec i3
            VERIFY startx auto launches i3
                > startx    (DO NOT RUN AS ROOT)
                [mod]+[shift]+[e] to EXIT i3 and kill Xorg


        have X11 launch when you login from tty1
            > nano ~/.bash_profile  (APPEND)
                if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
                    startx
                fi

        VERIFY xorg auto launch on login
            > logout
            log back in on tty1 and it should run startx (and i3) on login

=============================================
Graphical Display Manager (GDM) *optional*
============================================


lightdm (GDM: Graphical Display Manager)
    > sudo paru lightdm lightdm-gtk-greeter

ly (TUIDM: ncurses-like display manager)
    https://github.com/fairyglade/ly

agetty (already installed Terminal login screen)
    agetty
        [A]rch lo[G]in [T]ele[TY]pe
        Of course, replace "shahrose" with your username moving forward...

        Dont clear boot messages
            > nano /etc/systemd/system/getty@tty1.service.d/noclear.conf
                [Service]
                TTYVTDisallocate=no

        Completely hide boot messages / logging / errors
            add "quiet" to kernel boot message
            > nano /boot/loader/entries/arch.conf
                options root=PARTUUID=??????????? rw quiet

        TTY Session Count
            > nano /etc/systemd/logind.conf
                NAutoVTs=2    

        > ls /sys/firmware/efi/    (failure -> BIOS-serial console)
        SERIAL CONSOLE: auto login config
            > nano /etc/systemd/system/serial-getty@ttyS0.service.d/autologin.conf
                    [Service]
                    Type=simple    (good for auto login with no password+username)
(fix pollute)       Type=idle      (good for auto login with no username)
                    ExecStart=
(auto login)        ExecStart=-/usr/bin/agetty --autologin shahrose --noclear -s %I 115200,38400,9600 vt102
(auto user)         ExecStart=-/sbin/agetty --skip-login --login-options shahrose --noclear -s %I


        > ls /sys/firmware/efi/    (success -> UEFI-virtual console)
        VIRTUAL CONSOLE: auto login config
            > nano /etc/systemd/system/getty@tty1.service.d/autologin.conf
                    [Service]
                    Type=simple    (good for auto login with no password+username)
(fix pollute)       Type=idle      (good for auto login with no username)
                    ExecStart=
(auto login)        ExecStart=-/usr/bin/agetty --autologin shahrose --noclear %I $TERM
(auto user)         ExecStart=-/sbin/agetty --skip-login --login-options shahrose --noclear %I





=============================================
Desktop Environment Configs
============================================

Lock Screen (optional)
    > paru i3-lock
        Simple Password Lock (runs via Window Manager, i3)
        > paru i3lock-fancy  (has blur effects)
        to lock computer, run (> i3lock)

    > paru xsecurelock
        Security Focused Lock (runs via Display Server, Xorg)
        it is an Open Source project made by Google (likly to protect their own shit...)
        https://github.com/google/xsecurelock

        Authentication Module
            auth_x11
        Screen Saver Modules
            saver_blank:
                Simply blanks the screen.
            saver_mplayer // saver_mpv:
                Plays a video using mplayer or mpv, respectively. The video to play is selected at random among all files in ~/Videos.
            saver_multiplex:
                Watches the display configuration and runs another screen saver module once on each screen; used internally.
            saver_xscreensaver:
                Runs an XScreenSaver hack from an existing XScreenSaver setup. NOTE: some screen savers included by this may display arbitrary pictures from your home directory; if you care about this, either run xscreensaver-demo and disable screen savers that may do this, or stay away from this one!


Automatic Locking (on Suspend)
    Computer should suspend based on "SuspendTime" set in Xorg configs
    Closing Laptop lid should also trigger Suspend

    > paru xss-lock
        Lock computer automatically before computer suspends
        (xsecurelock) xss-lock -n /usr/lib/xsecurelock/dimmer -l -- xsecurelock
        (i3lock) xss-lock --transfer-sleep-lock -- i3lock --nofork
        (i3lock) xss-lock --transfer-sleep-lock -- i3lock-fancy --nofork
    Add this to start of i3 config to have it autolaunch
        exec --no-startup-id xss-lock -n /usr/lib/xsecurelock/dimmer -l -- xsecurelock


    (ALTENATIVE to xss-lock)
    > paru xautolock  
        xautolock -time 10 -notify 5 -notifier '/usr/lib/xsecurelock/until_nonidle /usr/lib/xsecurelock/dimmer' -locker xsecurelock

=============================================
Advanced Monitor Settings
============================================

Adjustable Screen Brightness (optional, recommended for Laptops)
    Brightness Control Program (CLI)
        > paru brightnessctl
        > brightnessctl s 50%   (set to 50% of max brightness)
        > brightnessctl s 10%+  (relative amounts work too)
        > brightnessctl -l      (get list of other devices that have brightnesses)
        > brightnessctl -d "mmc0::" s 25%    (set back-lit keyboard to 25%)

    Add Brightness Hotkey (i3)
        https://wiki.archlinux.org/title/Backlight

        If you have a laptop, you might have proprietary buttons (like brightness buttons)
        we can discover a key's true name using "xev"
        > paru xev
        > xev
            press any key while running this program and its "true name" will display in terminal
            Once we know the key's true name, we can bind it to a hotkey

        eg, if the key's true name is "XF86MonBrightnessDown"...
        > nano ~/.config/i3/config
            bindsym XF86MonBrightnessDown exec brightnessctl s 10%-
            bindsym XF86MonBrightnessUp exec brightnessctl s 10%+

        if you lose brightness control after waking from suspend...
            you may need to configure additional kernel parameters
            (by default, tells BIOS we are windows...we may need to tell the truth)
                acpi_osi=Linux
                acpi_osi="!Windows 2012"
                acpi_osi=
            you might also have to disable fastboot
                > nano /etc/modprobe.d/i915.conf
                    options i915 fastboot=0
            this can happen on Microsoft hardware (eg, Surface Laptops)

 
Hi-DPI Settings (if applicable)
    > paru xrandr
    > paru xrandr-extend
    > paru xlayoutdisplay    (supposed to automatically adjust for HIDPI displays?)

    > xrandr --output HDMI-0 --scale 1.5x1.5
    https://wiki.archlinux.org/index.php/Xorg#Display_size_and_DPI


Montior Resolution Tweaking (Xorg)
    xrandr    (CLI)
    arandr    (GUI)


Display Wiki Links
    https://wiki.archlinux.org/index.php/NVIDIA#Xorg_configuration
    https://wiki.archlinux.org/index.php/Xorg#Proprietary_NVIDIA_driver
    https://wiki.archlinux.org/index.php/Multihead#Configuration_using_xrandr
    https://wiki.archlinux.org/index.php/NVIDIA#Multiple_monitors



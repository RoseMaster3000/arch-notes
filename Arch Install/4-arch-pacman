=============================================
Preparing Package Managers
============================================
With our Kernel and Distro installed, we now have a terminal computer ready to go.
Now all we have to do is download/install our GUI software and work/game software.
"Technically" everything from this point on is optional, just download what you need.

Downloading/installation is painless if do the following things first:
    - configure Internet connection (and system time)
    - configure package manager (pacman)
    - install a package helper (yay and/or paru)

=============================================
Internet Connectivity
============================================

ENTER SUPER USER (optional, but if you don't you will be typing your password a ton)
    > su

CONNECT TO WIFI  (skip if you are using ethernet)
    If you are using ethernet, just plug it in

    If you are using WIFI, connect use iwd
        > systemctl start iwd
        > systemctl enable iwd  (so thats this starts on boot)
        > iwctl
            > ...
            I have experienced issues where iwctl would list no devices on boot?
            This can be fixed by restarting the iwd service.
            > systemctl restart iwd    
            This might solve the issue permenantly?
            > systemctl edit iwd.service
                [Service]
                ExecStartPre=/usr/bin/sleep

WIFI PCI cards
    > lspci -nnk | grep -iA3 net
    > lsusb

    modprobe -r iwlwifi
    sudo modprobe iwlwifi
    dmesg | grep iwlwifi



IP ADDRESS RESOLVER
    If you want router to assign IP automatically (DHCP)
        > dhcpcd                   (get ip address, one time run)
        > systemctl start dhcpcd   (get ip address in background)
        > systemctl enable dhcpcd  (^start on boot)

    If your router has a static address reserved for this computer
        > ip address
            record mac address
                (eg: 34:17:eb:be:55:b6)
                use when reserving IP on your router
            record Network dev name
                (eg: eno1)


        > nano /etc/systemd/network/20-wired.network (WRITE TO FILE:)
            [Match]
            Name=NETWORK-DEV-NAME    (eg: eno1)
            [Network]
            DNS=1.1.1.1              (optional DNS override)
            [Route]
            Gateway=192.168.86.1     (gateway of Router)
            Destination=/something   (gateway path to access router)
            [Address]
            Address=192.168.86.2/24  (static IP reserved by Router)

        > shorthand version of above
            [Match]
            Name=NETWORK-DEV-NAME
            [Network]
            Gateway=192.168.86.1     (gateway of Router)
            Address=192.168.86.2/24  (static IP reserved by Router)

        Device name not showing up on router???
            https://bbs.archlinux.org/viewtopic.php?id=263284


    VERIFY IT WORKED
        > ip address
        > ping 1.1.1.1

> doas nano /etc/systemd/resolved.conf
    [Resolve]
    DNS: 1.1.1.1
> systemctl restart systemd-resolved

Configure Time
    > timedatectl set-ntp true      (network time protocol)
    > timedatectl list-timezones
        BTW: these are all stored in /usr/share/zoneinfo/*
    > timedatectl set-timezone America/New_York
        (OR) Asia/Karachi
        BTW: this is stored in /etc/localtime (or is it? idk.)
    > hwclock --systohc
        update hardware clock
        this makes time settings perisitent accorss boot
    VERIFY
        > timedatectl status


=============================================
REMOTE MACHINE ACCESS (useful for servers)
============================================
SSH
    SERVER:
        > nano /etc/ssh/sshd_config
            Port XXXXX   (change it from 22 to something else)
            ensure you are not using a reserved port:
                https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
        > systemctl start sshd.service
        > systemctl enable sshd.service
        > ip a
        > curl –s https://icanhazip.com
    CLIENT:
        with
        > paru openssh
        > ssh -p XXXXX username@192.168.100.92
        > paru sshpass
        > sshpass -p "password" ssh -p XXXXX username@10.0.0.2

RDP
    https://wiki.archlinux.org/index.php/rdesktop
    https://wiki.archlinux.org/index.php/Xrdp

=============================================
PACKAGE MANAGERS
============================================

Pacman [the "package manager"]
    https://wiki.archlinux.org/title/pacman
        "pacman" is the package manager for Arch Linux, we can use it to install community verified software.
        > pacman -Syy    (make sure the packages you already have are up to date)

    Pacman configuration
        > nano /etc/pacman.conf (UNCOMMENT FOLLOWING:)
            # Misc Options
            Color                  (add color to pacman output)
            ILoveCandy             (fun loading bar :P)
            ...
            [multilib]
            Include = /etc/pacman.d/mirrorlist      (additional community libraries, like Steam)

    (to actaully fetch multilib packages > pacman -Sy 

Limitation of pacman:
    pacman only downloads from the community repository

    The "arch user repo" (AUR) is HUGE repository filled with anything users want to upload.
        (Although, AUR packages are not "official" so they can sometimes be unstable)
        https://aur.archlinux.org/packages/?O=0&SeB=nd&K=&outdated=&SB=p&SO=d&PP=50&do_Search=Go
    There are many other arch package repos (eg arch-biolinux for biotech, etc...)


    To use project from AUR/others we will need to use git to clone...
    We will also have to manage dependencies...q
    and then we will have to compile those projects using "makepkg"...
    This is tedious, which is why we download a "package helper!"


yay [package helper: option 1]
    "yay" is a tool that automates the process of installing/managing non-community packages.
 
    However, yay itself is an AUR package, so we cant just grab it with pacman
    We will have to compile it from scratch
    INSTALL
        > git clone https://aur.archlinux.org/yay.git
        > cd yay
        > makepkg -si

    USAGE
        (identical to pacman)
        > yay -Syy
        > yay -S discord


paru [package helper: option 2]
    Many of the maintainers behind have left yay to work on PARU.
    It improves upon yay in that it is a interactive CLI tool rather than a simple utility like pacman.

    However, yay itself is an AUR package, so we cant just grab it with pacman
    If you have yay, you can use that (yay -S paru)
    If not you do not have yay, we can just compile paru from scratch

    INSTALL:
        (clone & build as non-root user)
        > git clone https://aur.archlinux.org/paru.git
        > cd paru
        > makepkg -si         (DO NOT RUN THIS AS ROOT)
        (if you clone as root the folder will belong to root)
        (you will have to change ownership with:)
        (> chown -R username /home/username/folder)

    USAGE
        > paru               : Alias for paru -Syu (update all packages)
        > paru <package>     : Search and install <package>.
        > paru -Sua          : Upgrade AUR packages only
        > paru -Qua          : Print available AUR updates
        > paru -Gc <package> : Print the AUR comments of <package>

    CONFIG: Make most relevant search be at bottom (imo, makes more sense in terminal)
        > nano /etc/paru.conf   (UNCOMMENT FOLLOWING:)
            BottomUp
            [bin]
            Sudo=doas

    VERIFY
        > paru
            (update all)
        > paru discord
            (search for "discord", and enter interactive installer prompt)



REFLECTOR (Pacman Mirror Updater)
    https://xyne.archlinux.ca/projects/reflector/
        Copies of community packages are stored on servers around the world called "mirrors"
        You always want to be using up to date mirrors / mirrors close to your location.
        "reflector" is a package that automatically keeps the mirrors pacman uses up to date. 

    Install
        > paru reflector
        > cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
                (make backup of default mirrorlist)
        > reflector -c "US" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist
                (replace "US" with your country, eg "IN")

    Mirrorlist auto-update (on timer)
        > nano /etc/xdg/reflector/reflector.conf   (config file)
        > systemctl start refelctor.timer
        > systemctl enable reflector.timer         (reflector starts on boot)
        > systemctl list-timers

    Mirrorlist instant-update
        > systemctl start reflector.service
        > systemctl enable reflector.service


Optimize makepkg (package compiler)
    > lscpu | grep "CPU(s):"                 (see how many cpu cores we have...)
    > nano /etc/makepkg.conf (modify lines)
        COMPRESSXZ=(xz -c -T 4 -z -)         (I had 4 cores)
        ...
        MAKEFLAGS="-j5"                      (I had 4 cores, meaning 5 workers)


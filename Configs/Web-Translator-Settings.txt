{
    "timeStamp": 1663307064585,
    "version": "9.5.4",
    "pageTranslatorService": "google",
    "textTranslatorService": "google",
    "ttsSpeed": 1,
    "enableDeepL": "yes",
    "targetLanguage": "en",
    "targetLanguageTextTranslation": "en",
    "targetLanguages": [
        "en",
        "en",
        "en"
    ],
    "alwaysTranslateSites": [],
    "neverTranslateSites": [],
    "sitesToTranslateWhenHovering": [],
    "langsToTranslateWhenHovering": [],
    "alwaysTranslateLangs": [
        "ja",
        "ru"
    ],
    "neverTranslateLangs": [
        "en"
    ],
    "showTranslatePageContextMenu": "no",
    "showTranslateSelectedContextMenu": "no",
    "showButtonInTheAddressBar": "yes",
    "showOriginalTextWhenHovering": "no",
    "showTranslateSelectedButton": "yes",
    "showPopupMobile": "no",
    "useOldPopup": "yes",
    "darkMode": "yes",
    "popupBlueWhenSiteIsTranslated": "yes",
    "popupPanelSection": 1,
    "showReleaseNotes": "no",
    "dontShowIfPageLangIsTargetLang": "no",
    "dontShowIfPageLangIsUnknown": "no",
    "dontShowIfSelectedTextIsTargetLang": "no",
    "dontShowIfSelectedTextIsUnknown": "no",
    "hotkeys": {
        "hotkey-toggle-translation": "",
        "hotkey-translate-selected-text": "",
        "hotkey-swap-page-translation-service": "",
        "hotkey-show-original": "",
        "hotkey-translate-page-1": "",
        "hotkey-translate-page-2": "",
        "hotkey-translate-page-3": ""
    },
    "expandPanelTranslateSelectedText": "no",
    "translateTag_pre": "yes",
    "dontSortResults": "no",
    "translateDynamicallyCreatedContent": "yes",
    "autoTranslateWhenClickingALink": "no",
    "translateSelectedWhenPressTwice": "no",
    "translateTextOverMouseWhenPressTwice": "no",
    "translateClickingOnce": "yes"
}